<?php

class LoginCest {
  public function _before(AcceptanceTester $I) {
  }

  // Test that administrator user can login.
  public function adminLogin(AcceptanceTester $I) {

    $I->amOnPage('/');
    $I->see('UW customizable news feed.');
    $I->amOnPage('user/login');
    $I->fillField('name', 'admin');
    $I->fillField('pass', 'UW@News');
    $I->click('Log in');
    $I->see('News Dashboard');
  }

  // Test that authenticated user can login.
  public function authenticatedLogin(AcceptanceTester $I) {

    $I->amOnPage('/');
    $I->see('UW customizable news feed.');
    $I->amOnPage('user/login');
    $I->fillField('name', 'testuser');
    $I->fillField('pass', '1234');
    $I->click('Log in');
    $I->see('News Dashboard');
  }
}
