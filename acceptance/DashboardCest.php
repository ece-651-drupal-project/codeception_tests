<?php

class DashboardCest {

  public function _before(AcceptanceTester $I) {
  }

  // Test that only our layouts are present.
  public function dashboardLayouts(AcceptanceTester $I) {

    // Login as authenticated user.
    $I->amOnPage('user/login');
    $I->fillField('name', 'testuser');
    $I->fillField('pass', '1234');
    $I->click('Log in');

    // Ensure that override button appears.
    $I->amOnPage('/dashboard/uw_news_dashboard');
    $I->see('Override');

    // Go to override page and ensure our layouts appear.
    $I->amOnPage('/dashboard/uw_news_dashboard/override');
    $I->see('Edit layout for');
    $I->click('Add section');
    $I->see('Choose a layout for this section');

    // The layouts to test for.
    $layouts = [
      'One column',
      'Two columns',
      'Three columns',
      'Inverted "L" - right',
      'Inverted "L" - left',
    ];

    // Step through each layout and ensure that it appears
    // as an option to choose.
    foreach ($layouts as $layout) {
      $I->see($layout);
    }
  }

  // Test 1 column Dashboard.
  public function dashboardOneColumn(AcceptanceTester $I) {

    // Login as authenticated user and go to dashboard page.
    $I->amOnPage('user/login');
    $I->fillField('name', 'testuser');
    $I->fillField('pass', '1234');
    $I->click('Log in');
    $I->amOnPage('/dashboard/uw_news_dashboard/override');
    $I->see('Edit layout for');

    // Add a one column and ensure that it appears.
    $I->click('Add section');
    $I->see('Choose a layout for this section');
    $I->click('One column');
    $I->see('Configure section');
    $I->click('Add section', '.button');
    $I->see('Add block');
    $I->seeElement('.layout--uw-1-col');
    $I->seeElement('.layout__region--first');

    // Remove one column section.
    $I->click('Remove Section 1');
    $I->click('Remove');
  }

  // Test two column Dashboard.
  public function dashboardTwoColumns(AcceptanceTester $I) {

    // Login as authenticated user and go to dashboard page.
    $I->amOnPage('user/login');
    $I->fillField('name', 'testuser');
    $I->fillField('pass', '1234');
    $I->click('Log in');
    $I->amOnPage('/dashboard/uw_news_dashboard/override');

    // The available options for two column layout.
    $options = [
      'even-split',
      'larger-left',
      'larger-right',
    ];

    // Step through each of the options and ensure that they appear.
    foreach ($options as $option) {

      // Add the section with the option and ensure that they appear.
      $I->click('Add section');
      $I->click('Two columns');
      $I->selectOption('select[name="layout_settings[layout_settings][column_class]"]', $option);
      $I->click('Add section', '.button');
      $I->seeElement('.layout--uw-2-col');
      $I->seeElement('.' . $option);
      $I->seeElement('.layout__region--first');
      $I->seeElement('.layout__region--second');

      // Remove two column section.
      $I->click('Remove Section 1');
      $I->click('Remove');
    }
  }

  // Test three column Dashboard.
  public function dashboardThreeColumns(AcceptanceTester $I) {

    // Login as authenticated user and go to dashboard page.
    $I->amOnPage('user/login');
    $I->fillField('name', 'testuser');
    $I->fillField('pass', '1234');
    $I->click('Log in');
    $I->amOnPage('/dashboard/uw_news_dashboard/override');

    // The available options for three column layout.
    $options = [
      'even-split',
      'larger-left',
      'larger-middle',
      'larger-right',
    ];

    // Step through each of the options and ensure that they appear.
    foreach ($options as $option) {

      // Add three column and ensure that it appears.
      $I->click('Add section');
      $I->click('Three columns');
      $I->selectOption('select[name="layout_settings[layout_settings][column_class]"]', $option);
      $I->click('Add section', '.button');
      $I->see('Add block');
      $I->seeElement('.layout--uw-3-col');
      $I->seeElement('.' . $option);
      $I->seeElement('.layout__region--first');
      $I->seeElement('.layout__region--second');
      $I->seeElement('.layout__region--third');

      // Remove three column section.
      $I->click('Remove Section 1');
      $I->click('Remove');
    }
  }

  // Test Inverted L Right column Dashboard.
  public function dashboardInvertedLRight(AcceptanceTester $I) {

    // Login as authenticated user and go to dashboard page.
    $I->amOnPage('user/login');
    $I->fillField('name', 'testuser');
    $I->fillField('pass', '1234');
    $I->click('Log in');
    $I->amOnPage('/dashboard/uw_news_dashboard/override');

    // The available options for inverted L right layout.
    $options = [
      'even-split',
      'larger-left',
      'larger-right',
    ];

    // Step through each of the options and ensure that they appear.
    foreach ($options as $option) {
      $I->click('Add section');
      $I->click('Inverted "L" - right');
      $I->selectOption('select[name="layout_settings[layout_settings][column_class]"]', $option);
      $I->click('Add section', '.button');
      $I->seeElement('.layout--uw-inverted-l-right');
      $I->seeElement('.' . $option);
      $I->seeElement('.layout__region--first');
      $I->seeElement('.layout__region--second');
      $I->seeElement('.layout__region--third');
      $I->seeElement('.layout__region--fourth');

      // Remove inverted L right section.
      $I->click('Remove Section 1');
      $I->click('Remove');
    }
  }

  // Test Inverted L Left column Dashboard.
  public function dashboardInvertedLLeft(AcceptanceTester $I) {

    // Login as authenticated user and go to dashboard page.
    $I->amOnPage('user/login');
    $I->fillField('name', 'testuser');
    $I->fillField('pass', '1234');
    $I->click('Log in');
    $I->amOnPage('/dashboard/uw_news_dashboard/override');

    // The available options for inverted L left layout.
    $options = [
      'even-split',
      'larger-left',
      'larger-right',
    ];

    // Step through each of the options and ensure that they appear.
    foreach ($options as $option) {
      $I->click('Add section');
      $I->click('Inverted "L" - left');
      $I->selectOption('select[name="layout_settings[layout_settings][column_class]"]', $option);
      $I->click('Add section', '.button');
      $I->seeElement('.layout--uw-inverted-l-left');
      $I->seeElement('.' . $option);
      $I->seeElement('.layout__region--first');
      $I->seeElement('.layout__region--second');
      $I->seeElement('.layout__region--third');
      $I->seeElement('.layout__region--fourth');

      // Remove inverted L left section.
      $I->click('Remove Section 1');
      $I->click('Remove');
    }
  }
}
