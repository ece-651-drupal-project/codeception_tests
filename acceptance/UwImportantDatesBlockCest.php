<?php

class UwImportantDatesBlockCestBlockCest {

  public function _before(AcceptanceTester $I) {
  }

  // Test UW Important Dates Block.
  public function UwImportantDatesBlock(AcceptanceTester $I) {

    // Login as authenticated user.
    $I->amOnPage('user/login');
    $I->fillField('name', 'testuser');
    $I->fillField('pass', '1234');
    $I->click('Log in');

    // Go to dashboard page.
    $I->amOnPage('/dashboard/uw_news_dashboard/override');
    $I->see('Edit layout for');

    // Add a UW Important Dates with 4 max items selected as options.
    // Ensure that only 4 dates appear.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('UW Important Dates');
    $I->click('Add block');
    $I->see('UW Important Dates');

    // Remove UW News block.
    $I->click('Remove Section 1');
    $I->click('Remove');

    // Add a UW Important Dates 4 max items selected as options.
    // Ensure that only 4 dates appear.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('UW Important Dates');
    $I->selectOption('settings[max_items]', '4');
    $I->click('Add block');
    $I->seeNumberOfElements('.uwnews__wrapper', 4);
    $I->see('UW Important Dates');

    // Remove UW News block.
    $I->click('Remove Section 1');
    $I->click('Remove');

    // Add a UW Important Dates and ensure that validation is
    // working correctly, i.e. form errors appear.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('UW Important Dates');
    $I->fillField('settings[max_chars]', '1000');
    $I->click('Add block');
    $I->see('Maximum number of characters must be between 100 and 600.');
    $I->fillField('settings[max_chars]', 'zzzz');
    $I->click('Add block');
    $I->see('Maximum number of characters must be a number.');
    $I->fillField('settings[max_chars]', '400');
    $I->click('Add block');

    // Remove UW News block.
    $I->click('Remove Section 1');
    $I->click('Remove');
  }

}
