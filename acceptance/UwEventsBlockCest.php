<?php

class UwEventsBlockCest {

  public function _before(AcceptanceTester $I) {
  }

  // Test UW Events Block.
  public function UwEventsBlock(AcceptanceTester $I) {

    // Login as authenticated user.
    $I->amOnPage('user/login');
    $I->fillField('name', 'testuser');
    $I->fillField('pass', '1234');
    $I->click('Log in');

    // Go to dashboard page.
    $I->amOnPage('/dashboard/uw_news_dashboard/override');
    $I->see('Edit layout for');

    // Add a UW News block with All filter
    // and 4 max stories selected as options.  Ensure that only
    // 4 stories and filter is applied to block.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('UW Events');
    $I->click('Add block');
    $I->see('UW Events');

    // Remove UW News block.
    $I->click('Remove Section 1');
    $I->click('Remove');

    // Add a UW Events block 4 max stories selected as options.
    // Ensure that only 4 events is applied to block.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('UW Events');
    $I->selectOption('settings[max_items]', '4');
    $I->click('Add block');
    $I->seeNumberOfElements('.uwnews__wrapper', 4);
    $I->see('UW Events');

    // Remove UW News block.
    $I->click('Remove Section 1');
    $I->click('Remove');

    // Add a UW Events block and ensure that validation is
    // working correctly, i.e. form errors appear.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('UW Events');
    $I->fillField('settings[max_chars]', '1000');
    $I->click('Add block');
    $I->see('Maximum number of characters must be between 100 and 600.');
    $I->fillField('settings[max_chars]', 'zzzz');
    $I->click('Add block');
    $I->see('Maximum number of characters must be a number.');
    $I->fillField('settings[max_chars]', '400');
    $I->click('Add block');

    // Remove UW News block.
    $I->click('Remove Section 1');
    $I->click('Remove');
  }

}
