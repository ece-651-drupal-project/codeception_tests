<?php

class UwStoriesBlockCest {

  public function _before(AcceptanceTester $I) {
  }

  // Test UW News Block.
  public function UwStoriesBlock(AcceptanceTester $I) {

    // Login as authenticated user.
    $I->amOnPage('user/login');
    $I->fillField('name', 'testuser');
    $I->fillField('pass', '1234');
    $I->click('Log in');

    // Go to dashboard page.
    $I->amOnPage('/dashboard/uw_news_dashboard/override');
    $I->see('Edit layout for');

    // Add a UW stories block with Entrepreneurship (category filter)
    // and 4 max stories selected as options.  Ensure that only
    // 4 stories and filter is applied to block.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('UW Stories');
    $I->checkOption('input[name="settings[topics_list][topics][32]"]');
    $I->selectOption('select[name="settings[max_stories]"]', '4');
    $I->click('Add block');
    $I->seeNumberOfElements('.uwnews__wrapper', 4);
    $I->see('UW Stories');
    $I->see('Entrepreneurship');

    // Remove UW Stories block.
    $I->click('Remove Section 1');
    $I->click('Remove');

    // Add a UW stories block with Engineering (faculty filter)
    // and 4 max stories selected as options.  Ensure that only
    // 4 stories and filter is applied to block.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('UW Stories');
    $I->checkOption('input[name="settings[faculties_list][faculties][26]"]');
    $I->selectOption('select[name="settings[max_stories]"]', '4');
    $I->click('Add block');
    $I->seeNumberOfElements('.uwnews__wrapper', 4);
    $I->see('UW Stories');
    $I->see('Engineering');

    // Remove UW Stories block.
    $I->click('Remove Section 1');
    $I->click('Remove');

    // Add a UW stories block with current students (audience filter)
    // and 4 max stories selected as options.  Ensure that only
    // 4 stories and filter is applied to block.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('UW Stories');
    $I->checkOption('input[name="settings[audience_list][audience][46]"]');
    $I->selectOption('select[name="settings[max_stories]"]', '4');
    $I->click('Add block');
    $I->seeNumberOfElements('.uwnews__wrapper', 4);
    $I->see('UW Stories');
    $I->see('Current students');

    // Remove UW Stories block.
    $I->click('Remove Section 1');
    $I->click('Remove');

    // Add a UW stories block with engineering (story type filter)
    // and 4 max stories selected as options.  Ensure that only
    // 4 stories and filter is applied to block.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('UW Stories');
    $I->selectOption('select[name="settings[story_type_list][story_type]"]', 'Engineering');
    $I->selectOption('select[name="settings[max_stories]"]', '4');
    $I->click('Add block');
    $I->seeNumberOfElements('.uwnews__wrapper', 4);
    $I->see('UW Stories');
    $I->see('Engineering');

    // Remove UW Stories block.
    $I->click('Remove Section 1');
    $I->click('Remove');

    // Add a UW stories block with engineering (story type filter),
    // current students (audience filter) and 4 max stories selected
    // as options.  Ensure that only 4 stories and filter is
    // applied to block.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('UW Stories');
    $I->checkOption('input[name="settings[topics_list][topics][31]"]');
    $I->checkOption('input[name="settings[topics_list][topics][69]"]');
    $I->checkOption('input[name="settings[faculties_list][faculties][26]"]');
    $I->selectOption('select[name="settings[max_stories]"]', '4');
    $I->click('Add block');
    $I->seeNumberOfElements('.uwnews__wrapper', 4);
    $I->see('UW Stories');
    $I->see('Awards, Honours and Rankings, Research, Engineering');

    // Remove UW Stories block.
    $I->click('Remove Section 1');
    $I->click('Remove');

    // Add a UW stories block with multiple story types,
    // current students (audience filter) and 4 max stories selected
    // as options.  Ensure that only 4 stories and filter is
    // applied to block.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('UW Stories');
    $I->checkOption('input[name="settings[faculties_list][faculties][26]"]');
    $I->selectOption('select[name="settings[story_type_list][story_type]"]', 'Engineering');
    $I->selectOption('select[name="settings[max_stories]"]', '4');
    $I->click('Add block');
    $I->seeNumberOfElements('.uwnews__wrapper', 4);
    $I->see('UW Stories');
    $I->see('Engineering, Engineering');

    // Remove UW Stories block.
    $I->click('Remove Section 1');
    $I->click('Remove');
  }

}
