<?php

class UwGlobalNewsBlockCest {

  public function _before(AcceptanceTester $I) {
  }

  // Test UW Global News Block.
  public function UwGlobalNewsBlock(AcceptanceTester $I) {

    // Login as authenticated user.
    $I->amOnPage('user/login');
    $I->fillField('name', 'testuser');
    $I->fillField('pass', '1234');
    $I->click('Log in');

    // Go to dashboard page.
    $I->amOnPage('/dashboard/uw_news_dashboard/override');
    $I->see('Edit layout for');

    // The options for the global news block.
    $options = [
      'category' => [
        'filter_value' => 'Category: Business',
        'value' => 'business',
      ],
      'country' => [
        'filter_value' => 'Country: Canada',
        'value' => 'ca',
      ],
      'keywords' => [
        'filter_value' => 'Keywords (title and body): COVID',
        'value' => 'COVID',
      ],
      'keywords_title' => [
        'filter_value' => 'Keywords (title only): COVID',
        'value' => 'COVID',
      ],
      'source' => [
        'filter_value' => 'Source: CBC News',
        'value' => 'cbc-news',
      ],
    ];

    // Step through each of the options and ensure that they appear correctly.
    foreach ($options as $key => $option) {

      // Add a UW global news block with filters and 4 max stories
      // selected as options. Ensure that only 4 stories and filter
      // is applied to block.
      $I->click('Add section');
      $I->click('One column');
      $I->click('Add section', '.button');
      $I->click('Add block');
      $I->click('Global News');
      $I->selectOption('select[name="settings[filters][global_news_filter]"]', $key);

      // Depending on option either fill in field or use select.
      if ($key == 'keywords' || $key == 'keywords_title') {
        $I->fillField('input[name="settings[filters][' . $key . ']"]', $option['value']);
      }
      else {
        $I->selectOption('select[name="settings[filters][' . $key . ']"]', $option['value']);
      }

      $I->selectOption('select[name="settings[news_settings][max_items]"]', '4');
      $I->click('Add block');
      $I->seeNumberOfElements('.uwnews__wrapper', 4);
      $I->see('Global News');
      $I->see($option['filter_value']);
      $I->see('Language: All');

      // Remove UW Global News block.
      $I->click('Remove Section 1');
      $I->click('Remove');
    }

    // Add a UW global news block with filters with French as a language
    // and ensure that the language appears correctly.
    $I->click('Add section');
    $I->click('One column');
    $I->click('Add section', '.button');
    $I->click('Add block');
    $I->click('Global News');
    $I->selectOption('select[name="settings[filters][global_news_filter]"]', 'source');
    $I->selectOption('select[name="settings[filters][source]"]', 'google-news-fr');
    $I->selectOption('select[name="settings[news_settings][language]"]', 'fr');
    $I->selectOption('select[name="settings[news_settings][max_items]"]', '4');
    $I->click('Add block');
    $I->seeNumberOfElements('.uwnews__wrapper', 4);
    $I->see('Global News');
    $I->see('Source: Google News (France)');
    $I->see('Language: French');

    // Remove UW Global News block.
    $I->click('Remove Section 1');
    $I->click('Remove');
  }

}
