<?php

class CreateUsersCest {

  public function _before(AcceptanceTester $I) {
  }

  // Test UW Global News Block.
  public function CreateUsers(AcceptanceTester $I) {

    // Login as aministrator.
    $I->amOnPage('user/login');
    $I->fillField('name', 'admin');
    $I->fillField('pass', 'UW@News');
    $I->click('Log in');

    // Go to the create users page and ensure that we
    // can see the form.
    $I->amOnPage('/admin/create-users');
    $I->see('Create a bunch of users');

    // Fill in the number of users and test validation.
    $I->fillField(['name' => 'num_user'], '1000000');
    $I->click('Submit');
    $I->see('The number of users must be between 1 and 1000.');

    // Create 10 users.
    $I->fillField(['name' => 'num_user'], '10');
    $I->click('Submit');
    $I->see('Successfully created 10 users.');
  }

}
