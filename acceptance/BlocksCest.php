<?php

class BlocksCest {

  public function _before(AcceptanceTester $I) {
  }

  // Test to check that our blocks appear.
  public function checkBlocks(AcceptanceTester $I) {

    // Login as authenticated user.
    $I->amOnPage('user/login');
    $I->fillField('name', 'testuser');
    $I->fillField('pass', '1234');
    $I->click('Log in');

    // Go to dashboard page.
    $I->amOnPage('/dashboard/uw_news_dashboard/override');
    $I->see('Edit layout for');

    // Try and add a block, and ensure that everything
    // appears correctly.
    $I->click('Add section');
    $I->see('Choose a layout for this section');
    $I->click('One column');
    $I->see('Configure section');
    $I->click('Add section', '.button');
    $I->see('Add block');
    $I->click('Add block');
    $I->see('Choose a block');

    // Our blocks.
    $blocks = [
      'Global News',
      'UW News',
      'UW Stories',
      'UW events',
    ];

    // Step through each of our blocks and ensure that
    // they appear as a block to add.
    foreach ($blocks as $block) {
      $I->see($block);
    }

    // Remove the section.
    $I->amOnPage('/dashboard/uw_news_dashboard/override');
    $I->click('Remove Section 1');
    $I->click('Remove');
  }

}
