<?php

class UserRegistrationCest {

  public function _before(AcceptanceTester $I) {
  }

  // Test UW Global News Block.
  public function UserRegistration(AcceptanceTester $I) {

    // Go to the create a user page.
    $I->amOnPage('user/register');
  }

}
